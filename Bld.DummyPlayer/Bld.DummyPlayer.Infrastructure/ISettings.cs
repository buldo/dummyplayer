﻿using System.Security.Cryptography.X509Certificates;

namespace Bld.DummyPlayer.Infrastructure
{
    public interface ISettings
    {
        string AccessToken { get; set; }
    }
}