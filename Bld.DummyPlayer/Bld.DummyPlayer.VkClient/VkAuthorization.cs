﻿namespace Bld.DummyPlayer.VkClient
{
    using RestSharp;
    using RestSharp.Authenticators;

    internal class VkAuthorization : OAuth2Authenticator
    {
        public VkAuthorization(string accessToken)
            : base(accessToken)
        {
        }

        public override void Authenticate(IRestClient client, IRestRequest request)
        {
            request.AddParameter("access_token", AccessToken, ParameterType.GetOrPost);
        }
    }
}
