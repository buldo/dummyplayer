﻿namespace Bld.DummyPlayer.VkClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using RestSharp;
    using RestSharp.Authenticators;

    public class Client
    {
        private const string BASE_URL = "https://api.vk.com/method";

        private readonly Uri _baseUri;
        private IAuthenticator _authenticator;
        private User _currenUser;
        private List<User> _friends;

        public Client()
        {
            _baseUri = new Uri(BASE_URL);
        }

        public string AppId => "5113519";

        public User CurrenUser
        {
            get
            {
                if (_currenUser == null)
                {
                    RefreshCurrentUser();
                }

                return _currenUser;
            }
        }

        public List<User> Friends
        {
            get
            {
                if (_friends == null)
                {
                    RefreshFriends();
                }

                return _friends;
            }
        }

        /// <summary>
        /// Установка и проверка токена
        /// </summary>
        /// <param name="token">
        /// Токен авторизации
        /// </param>
        public void SetAccessToken(string token)
        {
            _authenticator = new VkAuthorization(token);
            RefreshCurrentUser();
        }

        public List<Track> GetTracks(User user)
        {
            var request = new RestRequest
            {
                Resource = "audio.get"

            };
            request.AddParameter("owner_id", user.Id, ParameterType.GetOrPost);

            var responces = Execute<List<VkResponse<Track>>>(request);
            return responces[0].Items;
        } 

        public void RefreshCurrentUser()
        {
            var request = new RestRequest
            {
                Resource = "users.get"
            };
            var users = Execute<List<User>>(request);
            var user = users.FirstOrDefault();
            _currenUser = user?.Id == 0 ? null : user;
        }

        public void RefreshFriends()
        {
            var request = new RestRequest
            {
                Resource = "friends.get"

            };
            request.AddParameter("user_id", CurrenUser.Id, ParameterType.GetOrPost);
            request.AddParameter("fields", "name", ParameterType.GetOrPost);
            request.AddParameter("order", "hints", ParameterType.GetOrPost);

            var responces = Execute<List<VkResponse<User>>>(request);
            _friends = responces[0].Items;
        }

        private T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient
                             {
                                 BaseUrl = _baseUri,
                                 Authenticator = _authenticator,
                                 Proxy = new WebProxy()
                             };

            request.AddParameter("v", "5.40", ParameterType.GetOrPost); // used on every request
            request.RootElement = "response";

            var response = client.Execute<T>(request);
            if (response.ErrorException != null)
            {
                string message = "Error retrieving response.  Check inner details for more info.";
                var exception = new ApplicationException(message, response.ErrorException);
                throw exception;
            }

            return response.Data;
        }
    }
}
