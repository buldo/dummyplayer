﻿namespace Bld.DummyPlayer.VkClient
{
    using System;

    public class Audio
    {
        public int Id { get; internal set; }

        public string Artist { get; internal set; }

        public string Title { get; internal set; }

        public int Duration { get; internal set; }

        public Uri Url { get; internal set; }
    }
}
