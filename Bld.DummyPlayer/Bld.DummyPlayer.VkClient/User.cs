﻿namespace Bld.DummyPlayer.VkClient
{
    using RestSharp.Deserializers;

    public class User
    {
        [DeserializeAs(Name = "id")]
        public long Id { get; set; }

        [DeserializeAs(Name = "first_name")]
        public string FirstName { get; internal set; }

        [DeserializeAs(Name = "last_name")]
        public string LastName { get; internal set; }

        //[DeserializeAs(Name = "last_name")]
        //public int RecordsCounter { get; internal set; }

        //public List<Audio> Records { get; } = new List<Audio>();
    }
}
