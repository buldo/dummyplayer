﻿using Bld.DummyPlayer.ViewModel.Notifications;

namespace Bld.DummyPlayer.ViewModel
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using Infrastructure;
    using Prism.Commands;
    using Prism.Interactivity.InteractionRequest;
    using VkClient;

    public class MainWindowViewModel
    {
        private readonly ISettings _setting;
        private readonly Client _vkClient;
        private readonly InteractionRequest<LoginConfirmation> _loginRequest = new InteractionRequest<LoginConfirmation>();
        private UserViewModel _selectedUser;

        public MainWindowViewModel(ISettings settings)
        {
            _setting = settings;
            _vkClient = new Client();
            HandleInterfaceLoadedCommand = new DelegateCommand(ProcessInterfaceLoadedCommand);
        }

        public ObservableCollection<UserViewModel> Users { get; } = new ObservableCollection<UserViewModel>();
        
        public ObservableCollection<TrackViewModel> Tracks { get; } = new ObservableCollection<TrackViewModel>();

        public UserViewModel SelectedUser
        {
            get
            {
                return _selectedUser;
            }

            set
            {
                _selectedUser = value;
                RefreshTracks();
            }
        }


        public IInteractionRequest LoginRequest => _loginRequest;

        public ICommand HandleInterfaceLoadedCommand { get; }

        private async void ProcessInterfaceLoadedCommand()
        {
            _vkClient.SetAccessToken(_setting.AccessToken);

            if (_vkClient.CurrenUser == null)
            {
                var confirmation =
                await _loginRequest.RaiseAsync(new LoginConfirmation
                {
                    Title = Localization.Properties.Resources.LoginString,
                    AppId = _vkClient.AppId
                });
                if (confirmation.Confirmed)
                {
                    _vkClient.SetAccessToken(confirmation.AccessToken);
                    _setting.AccessToken = confirmation.AccessToken;
                }
            }

            if (_vkClient.CurrenUser != null)
            {
                Users.Add(new UserViewModel(_vkClient.CurrenUser));
                _vkClient.Friends.ForEach(user => Users.Add(new UserViewModel(user)));
            }
        }

        private void RefreshTracks()
        {
            Tracks.Clear();
            var tracks = _vkClient.GetTracks(SelectedUser.Model);
            tracks.ForEach(track => Tracks.Add(new TrackViewModel(track)));
        }
    }
}
