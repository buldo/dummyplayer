﻿using Bld.DummyPlayer.VkClient;

namespace Bld.DummyPlayer.ViewModel
{
    using Prism.Mvvm;

    public class TrackViewModel : BindableBase
    {
        public TrackViewModel(Track track)
        {
            Title = track.Title;
            //FilePath = track.
        }

        public string Title { get; }

        public string FilePath { get; }
    }
}
