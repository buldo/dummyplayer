﻿using Bld.DummyPlayer.ViewModel.Notifications;

namespace Bld.DummyPlayer.ViewModel
{
    using System;
    using Prism.Interactivity.InteractionRequest;
    using Prism.Mvvm;

    public class LoginViewModel : BindableBase, IInteractionRequestAware
    {
        private string _appId = string.Empty;
        private string _accessToken = string.Empty;
        private LoginConfirmation _confirmation;

        public string AppId
        {
            get
            {
                return _appId;
            }

            set
            {
                SetProperty(ref _appId, value);
            }
        }

        public string AccessToken
        {
            get
            {
                return _accessToken;
            }

            set
            {
                SetProperty(ref _accessToken, value);
                if (!string.IsNullOrWhiteSpace(value))
                {
                    _confirmation.Confirmed = true;
                    _confirmation.AccessToken = value;
                    FinishInteraction();
                }
            }
        }

        public INotification Notification
        {
            get
            {
                return _confirmation;
            }

            set
            {
                _confirmation = (LoginConfirmation)value;
                OnPropertyChanged(() => Notification);
                AppId = _confirmation.AppId;
            }
        }

        public Action FinishInteraction { get; set; }
    }
}
