﻿namespace Bld.DummyPlayer.ViewModel.Notifications
{
    using Prism.Interactivity.InteractionRequest;

    public class LoginConfirmation : Confirmation
    {
        public string AppId { get; set; }

        public string AccessToken { get; set; }
    }
}
