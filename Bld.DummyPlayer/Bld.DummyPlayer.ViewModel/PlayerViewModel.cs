﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NAudio.Wave;
using Prism.Commands;
using Prism.Mvvm;

namespace Bld.DummyPlayer.ViewModel
{
    public class PlayerViewModel : BindableBase
    {
        private TrackViewModel _currentTrack;
        private IWavePlayer _wavePlayer;
        private WaveStream _reader;

        public TrackViewModel CurrentTrack
        {
            get
            {
                return _currentTrack;
            }

            private set
            {
                SetProperty(ref _currentTrack, value);
            }
        }

        public ICommand PlayPauseCommand { get; private set; }

        public void PlayTrack(TrackViewModel track)
        {
            CurrentTrack = track;

            PlayPauseCommand = new DelegateCommand(ExecutePlayPause);
        }

        private void ExecutePlayPause()
        {
            if (_wavePlayer == null)
            {
                CreatePlayer();
            }

            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }

            if (_reader == null)
            {
                _reader = new MediaFoundationReader(CurrentTrack.FilePath);
                _reader = new MediaFoundationReader(CurrentTrack.FilePath);
                _wavePlayer.Init(_reader);
            }

            _wavePlayer.Play();
        }

        private void CreatePlayer()
        {
            _wavePlayer = new WaveOutEvent();
            _wavePlayer.PlaybackStopped += WavePlayerOnPlaybackStopped;
        }

        private void WavePlayerOnPlaybackStopped(object sender, StoppedEventArgs stoppedEventArgs)
        {

            //if (_reader != null)
            //{
            //    SliderPosition = 0;
            //    //_reader.Position = 0;
            //    timer.Stop();
            //}
            //if (stoppedEventArgs.Exception != null)
            //{
            //    MessageBox.Show(stoppedEventArgs.Exception.Message, "Error Playing File");
            //}
            //OnPropertyChanged("IsPlaying");
            //OnPropertyChanged("IsStopped");
        }
    }
}
