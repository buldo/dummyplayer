﻿namespace Bld.DummyPlayer.ViewModel
{
    using Bld.DummyPlayer.VkClient;
    using System.Collections.ObjectModel;

    public class UserViewModel
    {
        public UserViewModel(User user)
        {
            Model = user;
            Name = user.FirstName + " " + user.LastName;
        }

        public string Name { get; }

        public string Avatar { get; }

        public User Model { get; }

        public ObservableCollection<TrackViewModel> Tracks { get; } = new ObservableCollection<TrackViewModel>();
    }
}