﻿using Bld.DummyPlayer.Infrastructure;
using Bld.DummyPlayer.ViewModel;
using Prism.Mvvm;

namespace Bld.DummyPlayer.Desktop
{
    using System.Windows;
    using Microsoft.Practices.Unity;
    using Prism.Unity;

    internal class DesktopBootstrapper : UnityBootstrapper
    {
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterInstance<ISettings>(new Settings());
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();
            ViewModelLocationProvider.Register(typeof(MainWindow).FullName, () => Container.Resolve<MainWindowViewModel>());
            ViewModelLocationProvider.Register(typeof(LoginView).FullName, () => Container.Resolve<LoginViewModel>());
        }

        protected override DependencyObject CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
            Application.Current.MainWindow = (MainWindow)Shell;
            Application.Current.MainWindow.Show();
        }
    }
}
